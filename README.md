# Gapps 

- Action Services (MatchmakerPrebuilt)
- ArCore
- Device Health Services (TurboPrebuilt)
- Google (Velvet)
- Google Connectivity Services (GCS)
- Google Dialer
- Google Recommendations (SettingsIntelligenceGooglePrebuilt)
- Google Play Store (Phonesky)
- Google VR Services (VRCore)
- Calendar
- Contacts
- Messages (PrebuiltBugle)
- Photos
- PrebuiltDeskClockGoogle
- Soundpicker
